defmodule CardsTest do
  use ExUnit.Case
  # running tests for properly formatted examples in documentation
  doctest Cards

  test "create_deck makes 52 cards" do
  	deck_length = length(Cards.create_deck)
  	assert deck_length == 52
  end

  test "shuffling a deck randomizes it" do
  	deck = Cards.create_deck
  	assert deck != Cards.shuffle(deck)
  	# different way of writing ^^
  	refute deck == Cards.shuffle(deck)
  end

end
