# Cards

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cards` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:cards, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cards](https://hexdocs.pm/cards).

`mix docs` - generate docs with :ex_doc and access it on doc/index.html

### Comparision AND assgnment with pattern matching - Hard stuff

- ["red", color] = ["red", "blue"] - SUCCESS
- ["green", color] = ["red", "blue"] - FAIL (no match MatchError)
- {:ok, binary} = {:ok, ["filename"]} - SUCCESS
- {:error, binary} = { :ok, ["filename"] } - FAIL

### How Pipe operator works
elixir automatically inject first argument into piped method
list_of_cards returned by Card.create_deck is injected as first arg into Crads.shuffle(deck)
list_of_cards returned by Card.shuffle is injected as first arg into Crads.deal(deck, hand_size)

NOTE: the arguments piped into must be consistent ( in this case the list )

```elixir
Cards.create_deck
|> Cards.shuffle
|> Cards.deal(hand_size)
```
