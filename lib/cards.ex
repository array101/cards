defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards
  """

  @doc """
    Returns a list of strings representing a deck of playing cards
  """
  def create_deck do
    values = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"]
    suits = ["Spades", "Clubs", "Hearts", "Diamond"]

    for suit <- suits, value <- values do
      "#{value} of #{suit}"
    end
  end

  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  @doc """
    Determines wheather a deck contains a given card

  ## Example

    iex> deck = Cards.create_deck
    iex> Cards.contains?(deck, "Ace of Spades")
    true

  """
  def contains?(deck, card) do
    Enum.member?(deck, card)
  end

  @doc """
    Divides a deck into a hand and the remainder of the deck.
    The `hand_size` argument indicates how many cards should be in the hand.

  ## Example

    iex> deck = Cards.create_deck
    iex> {hand, _deck} = Cards.deal(deck, 1)
    iex> hand
    ["Ace of Spades"]

  """
  def deal(deck, hand_size) do
    # returns a tuple
    Enum.split(deck, hand_size)
  end

  def save(deck, filename) do
    # built in erlang method
    binary = :erlang.term_to_binary(deck)
    # elixir code
    File.write(filename, binary)
  end

  def load(filename) do
    # the result is a tuple, then apply pattern matching
    case File.read(filename) do
      # comparision and assignment on a single step
      {:ok, binary} -> :erlang.binary_to_term(binary)
      {:error, _reason} -> "That file does not exist"
    end
  end

  def create_hand(hand_size) do
    # How Pipe operator works
    # elixir automatically inject first argument into piped method
    # list_of_cards returned by Card.create_deck is injected as first arg into Crads.shuffle(deck)
    # list_of_cards returned by Card.shuffle is injected as first arg into Crads.deal(deck, hand_size)
    #
    Cards.create_deck
    |> Cards.shuffle
    |> Cards.deal(hand_size)
  end

end

# Cards.deal(deck, 5) # elixir - { *hand*, *deck* }
# Access element on a tuple -

# Cards.deal(deck, 5) # ruby - { hand: [], deck: []}

# Comparision AND assgnment with pattern matching - Hard stuff
# ["red", color] = ["red", "blue"] - SUCCESS
# ["green", color] = ["red", "blue"] - FAIL (no match MatchError)
# {:ok, binary} = {:ok, ["filename"]} - SUCCESS
# {:error, binary} = { :ok, ["filename"] } - FAIL
#

# Pipe operator on def create_hand (|>)
# Cards.create_deck--> list_of_cards --Cards.shuffle--> list_of_cards --Cards.deal(hand_size)--> list_of_cards

